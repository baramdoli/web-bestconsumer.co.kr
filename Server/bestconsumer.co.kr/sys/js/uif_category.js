/**
 * 카테고리 마우스 오버 이미지
 * 카테고리 서브 메뉴 출력
 */

$(document).ready(function(){

    var methods = {
        aCategory    : [],
        aSubCategory : {},

        get: function()
        {
             $.ajax({
                url : '/exec/front/Product/SubCategory',
                dataType: 'json',
                success: function(aData) {

                    if (aData == null || aData == 'undefined') return;
                    for (var i=0; i<aData.length; i++)
                    {
                        var sParentCateNo = aData[i].parent_cate_no;

                        if (!methods.aSubCategory[sParentCateNo]) {
                            methods.aSubCategory[sParentCateNo] = [];
                        }

                        methods.aSubCategory[sParentCateNo].push( aData[i] );
                    }
					methods.Category_Menu_Print(); /* UI Fandango 추가 함수 */
					methods.Category_All_Print();
                }
            });
        },

        getParam: function(sUrl, sKey) {

            var aUrl         = sUrl.split('?');
            var sQueryString = aUrl[1];
            var aParam       = {};

            if (sQueryString) {
                var aFields = sQueryString.split("&");
                var aField  = [];
                for (var i=0; i<aFields.length; i++) {
                    aField = aFields[i].split('=');
                    aParam[aField[0]] = aField[1];
                }
            }
            return sKey ? aParam[sKey] : aParam;
        },

        show: function(overNode, iCateNo) {
			//alert("a:"+iCateNo);
            if (methods.aSubCategory[iCateNo].length == 0) {
				alert("b:"+iCateNo);
                return;
            }

            var aHtml = [];
            aHtml.push('<ul>');
            $(methods.aSubCategory[iCateNo]).each(function() {
                aHtml.push('<li><a href="/'+this.design_page_url+this.param+'">'+this.name+'</a></li>');
            });
            aHtml.push('</ul>');

            var offset = $(overNode).offset();
            $('<div class="sub-category"></div>')
                .appendTo(overNode)
                .html(aHtml.join(''))
                .find('li').mouseover(function(e) {
                    $(this).addClass('over');
                }).mouseout(function(e) {
                    $(this).removeClass('over');
                });

        },

        close: function() {
            $('.sub-category').remove();
        },
		
		/* UI Fandango 추가 함수 */
		/* 메인 메뉴 출력 */
        Category_Menu_Print: function() {

			$('#Category_Menu li').each(function() {

				var iCateNo = Number(methods.getParam($(this).find('a').attr('href'), 'cate_no'));
				var iCount = $(methods.aSubCategory[iCateNo]).length;
				if (iCount == 0) { return; }

				var aHtml = [];
				aHtml.push('<ul class="Category_Submenu">');
				$(methods.aSubCategory[iCateNo]).each(function() {
					aHtml.push('<li><a href="/'+this.design_page_url+this.param+'">'+this.name+'</a></li>');
				});
				aHtml.push('</ul>');
				$(aHtml.join('')).appendTo(this);

			});

        },

		/* 전체 메뉴 출력 */
        Category_All_Print: function() {

			$('#CateAll_1 li').each(function() {
				
				var aHtml = [];
				var dep = 2;
				var iCateNo = Number(methods.getParam($(this).find('a').attr('href'), 'cate_no'));
				var iCount = $(methods.aSubCategory[iCateNo]).length;
				//alert(iCateNo);
				$(this).addClass("CateAll_1_Sub");
				if (iCount > 0) {
					aHtml = methods.Category_All_Call(aHtml,dep,iCateNo);
					$(aHtml.join('')).appendTo(this);
				} else {
					return;
				}

			});

        },
		/* 전체 메뉴 세부 출력 */
        Category_All_Call: function(aHtml,dep,iCateNo) {
			//alert(iCateNo);
			var nowClass = "CateAll_" + dep;
			var nowClassSub = "CateAll_" + dep + "_Sub";
			aHtml.push('<ul class="'+nowClass+'">');

			$(methods.aSubCategory[iCateNo]).each(function() {
				
				var jCateNo = this.cate_no;
				var iCount = $(methods.aSubCategory[jCateNo]).length;

				aHtml.push('<li class="'+nowClassSub+'"><a href="/'+this.design_page_url+this.param+'">'+this.name+'</a>');
				
				/* 모든 카테고리를 출력하려면 아래 조건문에서 "&& dep < 2" 를 삭제 */
				if (iCount > 0 && dep < 2) {
					dep = dep + 1;
					aHtml = methods.Category_All_Call(aHtml,dep,jCateNo);
					dep = dep - 1;
				}

				aHtml.push('</li>');

			});
			
			aHtml.push('</ul>');

			return aHtml;

        }
    };

    methods.get();

	$('.xans-layout-category ul.Category_List li').mouseenter(function(e) {

		var $this = $(this).addClass('on'),
		iCateNo = Number(methods.getParam($this.find('a').attr('href'), 'cate_no'));
		if (!iCateNo) { return; }
		methods.show($this, iCateNo);

	}).mouseleave(function(e) {

		$(this).removeClass('on');
		methods.close();

	});

});
