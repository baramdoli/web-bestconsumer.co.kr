/* 상품 할인율 마킹 */
$(document).ready(function(){
	
	// 할인율 표시 켜기 on / 끄기 off
	var onoff = 'on';

	var pattern = /[^(0-9)]/gi; // 숫자이외는 제거
	var db,dparray;
	if(onoff == 'on'){
		$(".ImgBox").each( function() {
			if($(this).find(".data_price").text()){ 
				dp = $(this).find(".data_price").text();
				dparray = dp.split("/");

				for (var i=0; i<3; i++){
					if(i==2){ //상품할인금액 표시 될 경우 제거
						var dumy = dparray[i].split("(");
						dparray[i] = dumy[0];
					}
					if(pattern.test(dparray[i])){
						dparray[i] = dparray[i].replace(pattern,"");
					};
				};

				var listPrice = Number(dparray[1]);
				var salePrice = Number(dparray[2]);
				var priceCheck = dparray[3];
				var sRate = 0;
						
				//var ccc = listPrice + " / " + salePrice  + " / " + priceCheck;
				if(priceCheck != "T" && salePrice && listPrice > salePrice){
					//alert(ccc);
					sRate = Math.ceil((listPrice - salePrice)/(listPrice / 100));
					$(this).find( ".SaleRate_Mark" ).text(sRate + "%");
					$(this).find( ".SaleRate_Mark" ).removeClass( "off" );
				}else{
					$(this).find( ".SaleRate_Mark" ).addClass( "off" );
				};
			};
		});
	};

});
