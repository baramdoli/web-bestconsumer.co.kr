<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_MSHOP_SKIN_URL.'/style.css">', 0);

?>
<div id="main_bn_wr">
<?php
$max_width = $max_height = 0;
$bn_first_class = ' class="bn_first"';
$bn_sl = ' class="bn_sl"';

for ($i=0; $row=sql_fetch_array($result); $i++)
{
    if ($i==0) echo '<div id="main_bn" class="swipe">'.PHP_EOL;
    //print_r2($row);
    // 테두리 있는지
    $bn_border  = ($row['bn_border']) ? ' class="sbn_border"' : '';;
    // 새창 띄우기인지
    $bn_new_win = ($row['bn_new_win']) ? ' target="_blank"' : '';

    $bimg = G5_DATA_PATH.'/banner/'.$row['bn_id'];
    if (file_exists($bimg))
    {
        $banner = '';
        $size = getimagesize($bimg);

        if($size[2] < 1 || $size[2] > 16)
            continue;

        if($max_width < $size[0])
            $max_width = $size[0];

        if($max_height < $size[1])
            $max_height = $size[1];

        echo '<div class="item">'.PHP_EOL;
        if ($row['bn_url'][0] == '#')
            $banner .= '<a href="'.$row['bn_url'].'">';
        else if ($row['bn_url'] && $row['bn_url'] != 'http://') {
            $banner .= '<a href="'.G5_SHOP_URL.'/bannerhit.php?bn_id='.$row['bn_id'].'&amp;url='.urlencode($row['bn_url']).'"'.$bn_new_win.' >';
        
            echo $banner.'<span  style="background-image:url('.G5_DATA_URL.'/banner/'.$row['bn_id'].');" class="bn-img"></span>';
            if($banner)
                echo '</a>'.PHP_EOL;
        }
        else {
            echo $banner.'<span  style="background-image:url('.G5_DATA_URL.'/banner/'.$row['bn_id'].');" class="bn-img"></span>';
        }
        echo '</div>'.PHP_EOL;

        $bn_first_class = '';
        $bn_sl = '';

    }
}

if ($i > 0) {
    echo '</div>'.PHP_EOL;
    echo '<button type="button" class="bottom_btn">gdgdgd</button>'.PHP_EOL;

?>
</div>
<script>
$(document).ready(function() {
    $("#main_bn").owlCarousel({
        autoPlay : true,
        navigation : true, 
        pagination: false, 
        slideSpeed : 500,
        paginationSpeed : 400,
        autoPlay : 7000 ,
        singleItem:true,

    });
});

jQuery(document).ready(function($) {
    $(".bottom_btn").on("click", function() {           
        var position=$('#index').offset(); // 위치값
        $('html,body').animate({scrollTop:position.top},400); // 이동
    });
});


$(function() {
    function abso() {
        $('#main_bn').css({
            height: $(window).height()
        });

    }
    $(window).resize(function() {
        abso();         
    });

    abso();

});
</script>

<?php
}
?>