<?php
include_once('./_common.php');

define("_INDEX_", TRUE);

include_once(G5_THEME_MSHOP_PATH.'/shop.head.php');
?>


<?php echo display_banner('메인', 'mainbanner.10.skin.php'); ?>


<div id="index">
<div  id="idx_ev">
    <h2><span>EVENT</span></h2>
     <?php include_once(G5_MSHOP_SKIN_PATH.'/main.event.skin.php'); // 이벤트 ?>
</div>


<div id="idx_shop">

        <div class="idx_shop_wr">
            <h2><span>추천상품</span></h2>
      
            <?php if($default['de_mobile_type2_list_use']) { ?>
            <?php
            $list = new item_list();
            $list->set_mobile(true);
            $list->set_type(2);
            $list->set_view('it_id', false);
            $list->set_view('it_name', true);
            $list->set_view('it_cust_price', false);
            $list->set_view('it_price', true);
            $list->set_view('it_icon', true);
            $list->set_view('sns', false);
            echo $list->run();
            ?>
            <?php } ?>
        </div>
    

        <div class="idx_shop_wr">
          <h2><span>신상품</span></h2>
            <?php if($default['de_mobile_type3_list_use']) { ?>
            <?php
            $list = new item_list();
            $list->set_mobile(true);
            $list->set_type(3);
            $list->set_view('it_id', false);
            $list->set_view('it_name', true);
            $list->set_view('it_cust_price', false);
            $list->set_view('it_price', true);
            $list->set_view('it_icon', true);
            $list->set_view('sns', false);
            echo $list->run();
            ?>
            <?php } ?>
        </div>

    </div>

   <?php echo display_banner('왼쪽'); ?>
  
    
    <!-- 메인리뷰-->
    <?php
    // 상품리뷰
    $sql = " select a.is_id, a.is_subject, a.is_content, a.it_id, b.it_name
                from `{$g5['g5_shop_item_use_table']}` a join `{$g5['g5_shop_item_table']}` b on (a.it_id=b.it_id)
                where a.is_confirm = '1'
                order by a.is_id desc
                limit 0, 3 ";
    $result = sql_query($sql);

    for($i=0; $row=sql_fetch_array($result); $i++) {
        if($i == 0) {
            echo '<div id="idx_review">'.PHP_EOL;
            echo '<h2 ><span><a href="'.G5_SHOP_URL.'/itemuselist.php">사용후기</a></span></h2>'.PHP_EOL;
            echo '<ul>'.PHP_EOL;
        }

        $review_href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
    ?>
        <li class="rv_<?php echo $i;?>">
            <div class="rv_wr">
                <a href="<?php echo $review_href; ?>" class="rv_img"><?php echo get_itemuselist_thumbnail($row['it_id'], $row['is_content'], 300, 300); ?></a>
                <div class="rv_txt">
                    <span class="rv_tit"><?php echo get_text(cut_str($row['is_subject'], 20)); ?></span>
                    <a href="<?php echo $review_href; ?>" class="rv_prd"><?php echo get_text(cut_str($row['it_name'], 20)); ?></a>
                    <p><?php echo get_text(cut_str(strip_tags($row['is_content']), 90), 1); ?></p>
                    <a href="<?php echo $review_href; ?>" class="prd_view">상품보기 +</a>
                </div>
            </div>
        </li>
    <?php
    }

    if($i > 0) {
        echo '</ul>'.PHP_EOL;
        echo '</div>'.PHP_EOL;
    }
    ?>
    <!-- 메인리뷰-->

     <div id="idx_magazine">
        <?php
        // 이 함수가 바로 최신글을 추출하는 역할을 합니다.
        // 사용방법 : latest(스킨, 게시판아이디, 출력라인, 글자수);
        // 테마의 스킨을 사용하려면 theme/basic 과 같이 지정
        $options = array(
            'thumb_width'    => 500, // 썸네일 width
            'thumb_height'   => 300,  // 썸네일 height
            'content_length' => 20   // 간단내용 길이
        );
        echo latest('theme/magazine', 'gallery', 3, 40, 1, $options);
        ?>
    </div>



</div>

<script>
    $("#container").removeClass("container").addClass("idx-container");
</script>

<?php
include_once(G5_THEME_MSHOP_PATH.'/shop.tail.php');
?>