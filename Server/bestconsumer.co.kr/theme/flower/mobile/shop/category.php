<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

function get_mshop_category($ca_id, $len)
{
    global $g5;

    $sql = " select ca_id, ca_name from {$g5['g5_shop_category_table']}
                where ca_use = '1' ";
    if($ca_id)
        $sql .= " and ca_id like '$ca_id%' ";
    $sql .= " and length(ca_id) = '$len' order by ca_order, ca_id ";

    return $sql;
}
?>

<div id="category">

    <div id="tnb">
         <ul>
            <?php if ($is_member) { ?>

            <li><a href="<?php echo G5_BBS_URL; ?>/logout.php?url=shop">로그아웃</a></li>
            <li><a href="<?php echo G5_SHOP_URL; ?>/mypage.php">마이페이지</a></li>
            <?php } else { ?>
            <li><a href="<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo $urlencode; ?>">로그인</a></li>
            <li><a href="<?php echo G5_BBS_URL ?>/register.php" id="snb_join">회원가입</a></li>
            <?php } ?>
            <li><a href="<?php echo G5_SHOP_URL; ?>/couponzone.php">쿠폰존</a></li>
        </ul>
    </div>
    <div id="hd_sch">
        <h3>쇼핑몰 검색</h3>
        <form name="frmsearch1" action="<?php echo G5_SHOP_URL; ?>/search.php" onsubmit="return search_submit(this);">
        <label for="sch_str" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
        <input type="text" name="q" value="<?php echo stripslashes(get_text(get_search_string($q))); ?>" id="sch_str" required>
        <button type="submit"  id="sch_submit"><i class="fa fa-search" aria-hidden="true"></i><span class="sound_only">검색</span></button>
        </form>
        <script>
        function search_submit(f) {
            if (f.q.value.length < 2) {
                alert("검색어는 두글자 이상 입력하십시오.");
                f.q.select();
                f.q.focus();
                return false;
            }

            return true;
        }
        </script>

    </div>
    <div class="shop_cate">
        <h2 class="con_tit"><span>쇼핑몰분류</span></h2>
        <?php
        $mshop_ca_href = G5_SHOP_URL.'/list.php?ca_id=';
        $mshop_ca_res1 = sql_query(get_mshop_category('', 2));
        for($i=0; $mshop_ca_row1=sql_fetch_array($mshop_ca_res1); $i++) {
            if($i == 0)
                echo '<ul class="cate">'.PHP_EOL;
        ?>
        <li>
            <a href="<?php echo $mshop_ca_href.$mshop_ca_row1['ca_id']; ?>"><?php echo get_text($mshop_ca_row1['ca_name']); ?></a>
            <?php
            $mshop_ca_res2 = sql_query(get_mshop_category($mshop_ca_row1['ca_id'], 4));
            if(sql_num_rows($mshop_ca_res2))
                echo '<button class="sub_ct_toggle ct_op">'.get_text($mshop_ca_row1['ca_name']).' 하위분류 열기</button>'.PHP_EOL;

            for($j=0; $mshop_ca_row2=sql_fetch_array($mshop_ca_res2); $j++) {
                if($j == 0)
                    echo '<ul class="sub_cate sub_cate1">'.PHP_EOL;
            ?>
                <li>
                    <a href="<?php echo $mshop_ca_href.$mshop_ca_row2['ca_id']; ?>"><?php echo get_text($mshop_ca_row2['ca_name']); ?></a>
                    <?php
                    $mshop_ca_res3 = sql_query(get_mshop_category($mshop_ca_row2['ca_id'], 6));
                    if(sql_num_rows($mshop_ca_res3))
                        echo '<button type="button" class="sub_ct_toggle ct_op">'.get_text($mshop_ca_row2['ca_name']).' 하위분류 열기</button>'.PHP_EOL;

                    for($k=0; $mshop_ca_row3=sql_fetch_array($mshop_ca_res3); $k++) {
                        if($k == 0)
                            echo '<ul class="sub_cate sub_cate2">'.PHP_EOL;
                    ?>
                        <li>
                            <a href="<?php echo $mshop_ca_href.$mshop_ca_row3['ca_id']; ?>"><?php echo get_text($mshop_ca_row3['ca_name']); ?></a>
                            <?php
                            $mshop_ca_res4 = sql_query(get_mshop_category($mshop_ca_row3['ca_id'], 8));
                            if(sql_num_rows($mshop_ca_res4))
                                echo '<button type="button" class="sub_ct_toggle ct_op">'.get_text($mshop_ca_row3['ca_name']).' 하위분류 열기</button>'.PHP_EOL;

                            for($m=0; $mshop_ca_row4=sql_fetch_array($mshop_ca_res4); $m++) {
                                if($m == 0)
                                    echo '<ul class="sub_cate sub_cate3">'.PHP_EOL;
                            ?>
                                <li>
                                    <a href="<?php echo $mshop_ca_href.$mshop_ca_row4['ca_id']; ?>"><?php echo get_text($mshop_ca_row4['ca_name']); ?></a>
                                    <?php
                                    $mshop_ca_res5 = sql_query(get_mshop_category($mshop_ca_row4['ca_id'], 10));
                                    if(sql_num_rows($mshop_ca_res5))
                                        echo '<button type="button" class="sub_ct_toggle ct_op">'.get_text($mshop_ca_row4['ca_name']).' 하위분류 열기</button>'.PHP_EOL;

                                    for($n=0; $mshop_ca_row5=sql_fetch_array($mshop_ca_res5); $n++) {
                                        if($n == 0)
                                            echo '<ul class="sub_cate sub_cate4">'.PHP_EOL;
                                    ?>
                                        <li>
                                            <a href="<?php echo $mshop_ca_href.$mshop_ca_row5['ca_id']; ?>"> <?php echo get_text($mshop_ca_row5['ca_name']); ?></a>
                                        </li>
                                    <?php
                                    }

                                    if($n > 0)
                                        echo '</ul>'.PHP_EOL;
                                    ?>
                                </li>
                            <?php
                            }

                            if($m > 0)
                                echo '</ul>'.PHP_EOL;
                            ?>
                        </li>
                    <?php
                    }

                    if($k > 0)
                        echo '</ul>'.PHP_EOL;
                    ?>
                </li>
            <?php
            }

            if($j > 0)
                echo '</ul>'.PHP_EOL;
            ?>
        </li>
    <?php
    }

    if($i > 0)
        echo '</ul>'.PHP_EOL;
    else
        echo '<p>등록된 분류가 없습니다.</p>'.PHP_EOL;
    ?>
    </div>
    <div id="cate_sns">
        <h2>sns 링크</h2>
        <?php
        $save_file = G5_DATA_PATH.'/cache/theme/flower/snslink.php';
        if(is_file($save_file))
            include($save_file);
        ?>
        <?php if(isset($snslink['facebook']) && $snslink['facebook']) { ?>
        <a href="<?php echo set_http($snslink['facebook']); ?>" class="sns_f" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i><span class="sound_only">페이스북</span></a>
        <?php } ?>
        <?php if(isset($snslink['twitter']) && $snslink['twitter']) { ?>
        <a href="<?php echo set_http($snslink['twitter']); ?>" class="sns_t" target="_blank" ><i class="fa fa-twitter" aria-hidden="true"></i><span class="sound_only">트위터</span></a>
        <?php } ?>
        <?php if(isset($snslink['instagram']) && $snslink['instagram']) { ?>
        <a href="<?php echo set_http($snslink['instagram']); ?>" target="_blank" class="sns_i"><i class="fa fa-instagram" aria-hidden="true"></i><span class="sound_only">인스타그램</span></a>
        <?php } ?>

    </div>

   <button type="button" class="category_close"><i class="fa fa-times" aria-hidden="true"></i><span class="sound_only">카테고리 닫기</span></button>

</div>

<script>
$(function (){

    var $category = $("#category");

    $("#hd_ct").on("click", function() {
        $category.css("display","block");
    });

    $("#category .category_close").on("click", function(){
        $category.css("display","none");
    });

     $(".cate_bg").on("click", function() {
        $category.css("display","none");
    });

    $("button.sub_ct_toggle").on("click", function() {
        var $this = $(this);
        $sub_ul = $(this).closest("li").children("ul.sub_cate");

        if($sub_ul.size() > 0) {
            var txt = $this.text();

            if($sub_ul.is(":visible")) {
                txt = txt.replace(/닫기$/, "열기");
                $this
                    .removeClass("ct_cl")
                    .text(txt);
            } else {
                txt = txt.replace(/열기$/, "닫기");
                $this
                    .addClass("ct_cl")
                    .text(txt);
            }

            $sub_ul.toggle();
        }
    });
});
   
</script>
