<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

include_once(G5_THEME_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
set_cart_id(0);
$tmp_cart_id = get_session('ss_cart_id');

add_javascript('<script src="'.G5_THEME_JS_URL.'/owl.carousel.min.js"></script>', 10);
add_stylesheet('<link rel="stylesheet" href="'.G5_THEME_JS_URL.'/owl.carousel.css">', 10);

?>
<header id="hd_wr">
    <?php if ((!$bo_table || $w == 's' ) && defined('_INDEX_')) { ?><h1><?php echo $config['cf_title'] ?></h1><?php } ?>

    <div id="skip_to_container"><a href="#container">본문 바로가기</a></div>

    <?php if(defined('_INDEX_')) { // index에서만 실행
        include G5_MOBILE_PATH.'/newwin.inc.php'; // 팝업레이어
    } ?>
    <div id="hd" class="nav"> 
        <div id="logo"><a href="<?php echo G5_SHOP_URL; ?>/"><img src="<?php echo G5_DATA_URL; ?>/common/mobile_logo_img" alt="<?php echo $config['cf_title']; ?> 메인"></a></div>
        <?php include_once(G5_MSHOP_SKIN_PATH.'/boxcategory.skin.php'); // 상품분류 ?>

        <ul id="hd_icon">
            <li>
                <button type="button" class="menu-btn" id="menu_btn"><i class="fa fa-bars" aria-hidden="true"></i><span class="sound_only">전체메뉴</span></button>
            </li>
             <li><a href="<?php echo G5_SHOP_URL; ?>/mypage.php" class="menu-btn"><i class="fa fa-user" aria-hidden="true"></i><span class="sound_only">마이페이지</span></a></li>
            <li><a href="<?php echo G5_SHOP_URL; ?>/cart.php" class="menu-btn cart_btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="sound_only">장바구니</span><span class="cart-count"><?php echo get_cart_count($tmp_cart_id); ?></span></a></li>
     
        </ul>
        

        <?php include_once(G5_THEME_MSHOP_PATH.'/category.php'); // 분류 ?>

        
    </div>

    <script>
    $("#menu_btn").on("click", function() {
        $("#category").toggle();
    });

    $(document).mouseup(function (e){
        var container = $("#category");
        if( container.has(e.target).length === 0)
        container.hide();
    });


    $(window).scroll(function(){
      var sticky = $('.nav'),
          scroll = $(window).scrollTop();

      if (scroll >= 10) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
    </script>

    <div id="admin_link">
        <?php if ($is_admin) {  ?>
        <a href="<?php echo G5_ADMIN_URL ?>/shop_admin" target="_blank"><b>관리자</b></a>
        <a href="<?php echo G5_THEME_ADM_URL; ?>/" target="_blank"><b>테마관리</b></a>
        <?php } ?>

    </div>
</header>

<div id="container" class="container">

    <div id="container_wr">

    
    <?php
    if(basename($_SERVER['SCRIPT_NAME']) == 'faq.php')
        $g5['title'] = '고객센터';
    ?>


        <?php if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><h1 id="container_title"><span><?php echo $g5['title'] ?></span></h1><?php } ?>
