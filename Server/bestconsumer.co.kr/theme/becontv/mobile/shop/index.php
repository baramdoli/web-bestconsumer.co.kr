<?php
include_once('./_common.php');

define("_INDEX_", TRUE);

include_once(G5_THEME_MSHOP_PATH.'/shop.head.index.php');


//오른쪽 두개의 배너 가져오기
$sql = "select * from g5_shop_banner where bn_position='왼쪽' order by bn_begin_time DESC limit 2";
$result = sql_query($sql);
$i = 0;
while($row = sql_fetch_array($result)){
	$bann[$i]  = $row;
	$i++;
}
?>


<!-- 바디 레이아웃 -->
<div id="UIF_LayoutWrap">

		<div id="UIF_ContentsWrap">

			<!-- 컨텐츠 영역 -->
			<div id="UIF_Contents">
				<div class="MainBannerNotice">
                   <div class="moviebanner">
                     <!--<img src="img/main/main_movie.jpg" />-->
<?php
$sql = "select * from g5_shop_banner where bn_position='메인' order by bn_id DESC limit 1";
$result = sql_query($sql);
$row = sql_fetch_array($result);
?>
											<iframe width="795" height="420" src="https://www.youtube.com/embed/<?php echo $row['bn_alt']?>?autoplay=1" frameborder="0" allowfullscreen id="prod_video"></iframe>	

                   </div>
                   <div class="banner_right">
                      <ul>
<?php 
$sql = "select * from g5_shop_banner order by bn_id DESC limit 3 ";
$result = sql_query($sql);
while($row = sql_fetch_array($result)){
?>	
                        <li><a href="<?php echo $row['bn_url']?>"><img src="<?php echo G5_DATA_URL?>/banner/<?php echo $row['bn_id']?>" width="290" height="140"></a></li>
<?php	
}
?>
                      </ul>
                   </div>
                   <div class="banner_wide">

                    <a href=""><img src="img/main/center_banner.jpg" /></a>
                   </div>
                </div>
            
		<div class="Item_Title">
			<span class="Title_Txt">New Arrival</span>
			<div class="Title_Line"></div>
		</div>
                <!-- 메인 상품 롤링 : 메인 상품 모듈을 바꾸면 원하는 메인노출 상품 그룹이 출력됩니다. -->
<div class="UIF_ProductWrap_Slick clearboth">
    <ul class="ProductList_Slick">
<?php
$sql = "select * from g5_shop_item where it_use=1 and it_stock_qty > 0 order by it_id DESC limit 5";
$result = sql_query($sql);
while($row = sql_fetch_array($result)){
?>	
        <li class="Item_Slick">
			<a href="/shop/item.php?it_id=<?php echo $row['it_id']?>">
				<div class="ImgBox ImgBox_Slick">
					<!--<img src="img/main/main_prd_1.jpg"class="Thumb" />-->
					<?php echo get_it_image($row['it_id'], 230,230)?>
				</div>  
			</a>
			<div class="InfoBox_Slick">
				<p class="ProductName_Slick">
					<a href="/shop/item.php?it_id=<?php echo $row['it_id']?>"><span style="color:#fff"><?php echo $row['it_name']?></span></a>
				</p>
				<div module="product_ListItem" class="ProductItemBox_Slick">
					<p><strong style="color:#fff">가격 : <?php echo $row['it_price']?>원</strong></p>
				</div>
			</div>
        </li>
<?php
}
?>
    </ul>
</div><!-- //메인 상품 롤링 -->

<!-- 메인 상품 롤링 스크립트 : 아래 스크립트를 삭제할 경우 메인 상품롤링 기능이 작동하지 않고 화면이 깨질 수 있습니다. -->
<!-- slidesToShow 옵션은 상품 출력 갯수입니다. 4, 5로 최적화 되어 있으며 갯수 변경시 uif_layout.css 해당 스타일의 넓이 높이를 함께 수정해 주세요. -->
<script>
$('.ProductList_Slick').slick({
  slidesToShow: 5,
  slidesToScroll: 2,
  autoplay: true,
  autoplaySpeed: 4000,
});
</script><!-- //메인 상품 롤링 스크립트 -->


		<div class="Item_Title">
			<span class="Title_Txt">Weekly Best</span>
			<div class="Title_Line"></div>
		</div>
        <div class="main_weekly">
          <ul>
<?php
$sql = "select * from g5_shop_item where it_use=1 and it_stock_qty > 0 order by it_hit ASC limit 2";
$result = sql_query($sql);
while($row = sql_fetch_array($result)){
?>	
          <li>
             <a href="/shop/item.php?it_id=<?php echo $row['it_id']?>">
             <p><?php echo get_it_image($row['it_id'], 520)?></p>
             <p style="margin-top:20px"><?php echo $row['it_name']?><br /><?php echo $row['it_price']?>원</p>
           		</a>
          </li>

<?php	
}
?>
          </ul>
        </div>
        
        
        <div class="main_all">
          <ul>
<?php
$sql = "select * from g5_shop_item where it_use=1 and it_stock_qty > 0 order by it_id DESC";
$result = sql_query($sql);
while($row = sql_fetch_array($result)){
?>	
          <li>
             <a href="/shop/item.php?it_id=<?php echo $row['it_id']?>">
             <p><?php echo get_it_image($row['it_id'], 520)?></p>
             <p style="margin-top:20px"><?php echo $row['it_name']?><br /><?php echo $row['it_price']?>원</p>
           		</a>
          </li>

<?php	
}
?>
          </ul>
        </div>
			</div>
            
			<!-- //컨텐츠 영역 -->

		</div>
</div>
<!-- //바디 레이아웃 -->

<!-- 커뮤니티 레이아웃 -->
<!-- 이 영역은 모든 페이지 하단에 노출되는 커뮤니티 영역입니다. -->
<div id="UIF_CommWrap" class="clearboth">
	<div class="UIF_InnerWrap">
		<h2 class="CommTitle">Shop Community</h2>
		<div class="CommSection">
			<!-- 공지사항 게시판 -->
			<div class="CommBoard">
				<h3>Shop Notice</h2>
				<p class="Comment">쇼핑몰 공지사항 입니다.</h2>
				<table border="1" summary="">
					<caption>공지사항</caption>
					<tbody>
<?php
$sql = "select * from g5_write_notice order by wr_id DESC";
$result = sql_query($sql);
while($row = sql_fetch_array($result)){
?>
						<tr>
							<td class="title"><a href="/bbs/board.php?bo_table=notice&wr_id=<?php echo $row['wr_id']?></?php>"><?php echo $row['wr_subject']?></a></td>
							<td><?php echo substr($row['wr_datetime'],0,10)?></td>
						</tr>
<?php	
}
?>
					</tbody>
				</table>
				<p class="more"><a href="/bbs/board.php?bo_table=notice&page=" class="UIF_BtnTypeA BSet_Black01" style="color:white;">더보기</a></p>
			</div>
			<!-- //공지사항 게시판 -->

			<!-- FAQ 게시판 -->
			<div class="CommBoard">
				<h3>FAQ</h2>
				<p class="Comment">자주묻는 질문 답변입니다</h2>
				<table border="1" summary="">
					<caption>FAQ</caption>
					<tbody>
<?php
$sql = "select * from g5_faq where fm_id=1 order by fa_order DESC,fa_id DESC";
$result = sql_query($sql);
while($row = sql_fetch_array($result)){
?>
						<tr>
							<td class="title"><a href="/bbs/faq.php"><?php echo $row['fa_subject']?></a></td>
						</tr>
<?php	
}
?>
					</tbody>
				</table>
				<p class="more"><a href="/bbs/faq.php" class="UIF_BtnTypeA BSet_Black01" style="color:white;">더보기</a></p>
			</div>
			<!-- //FAQ 게시판 -->

			<!-- 고객센터 -->
			<div class="CommInfo">
				<h3>Customer Service</h2>
				<p class="Comment">항상 고객님이 최우선입니다</h2>
				<div module="Layout_Info" class="CustomorInfo">
					<ul class="TypeA">
						<li class="ShopTel"><span>070-1234-5678</span></li>
						<li><span class="fa-fax">FAX 02-1234-5678</span></li>
					</ul>
					<ul class="TypeB">
						<li><span class="fa-clock-o">오전 9시 ~ 오후 6시. 토,일,공휴일 휴무</span></li>
						<li><span class="fa-check-square-o">국민은행 1234-12345-1234456</span></li>
						<li><span class="fa-blank">국민은행 1234-12345-1234456</span></li>
						<li><span class="fa-blank">예금주 : 홍길동</span></li>
					</ul>
				</div>
				<div class="CommSNS">
					<a href="#"><img src="img/comm/uif_sns_facebook.png" /></a>
					<a href="#"><img src="img/comm/uif_sns_twitter.png" /></a>
					<a href="#"><img src="img/comm/uif_sns_instagram.png" /></a>
					<a href="#"><img src="img/comm/uif_sns_naverblog.png" /></a>
					<a href="#"><img src="img/comm/uif_sns_kakaostroy.png" /></a>
				</div>
			</div>
			<!-- //고객센터 -->
		</div>
	</div>
</div>
<!-- //커뮤니티 레이아웃 -->


<?php
include_once(G5_THEME_MSHOP_PATH.'/shop.tail.php');
?>