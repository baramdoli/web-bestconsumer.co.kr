
/* 푸쉬 메뉴 */
window.onload = function() {

	/*
	var MenuHeight = $(".UIF_PushMenu_Wrap").height();
	MenuHeight = MenuHeight + "px";
	MenuHeightIv = "-" + MenuHeight;
	$("#UIF_PushMenu").css("height",MenuHeight);
	$("#UIF_PushMenu").css("top",MenuHeightIv);

	var CateAll_1_Height = $("ul.CateAll_1").height();
	CateAll_1_Height = CateAll_1_Height + "px";
	$("ul.CateAll_1 > li").css("height",CateAll_1_Height);
	*/

	var menuTop = document.getElementById( 'UIF_PushMenu' ),
		showTop = document.getElementById( 'UIF_PushMenu_ShowTop' ),
		allCloseBtn = document.getElementById( 'UIF_PushMenu_Close' ),
		allCloseBtn2 = document.getElementById( 'UIF_PushMenu_Close2' ),
		body = document.body;

	showTop.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( menuTop, 'UIF_PushMenu-open' );
		classie.toggle( body, 'overflowhidden' );
	};
	allCloseBtn.onclick = function() {
		classie.remove( menuTop, 'UIF_PushMenu-open' );
		classie.remove( body, 'overflowhidden' );
	};
	allCloseBtn2.onclick = function() {
		classie.remove( menuTop, 'UIF_PushMenu-open' );
		classie.remove( body, 'overflowhidden' );
	};

	$("#spi1_2").text($("#spi1_1 > span").text());
	$("#spi2_2").text($("#spi2_1 > span").text());
	$("#spi3_2").text($("#spi3_1 > span").text());
	$("#spi4_2").text($("#spi4_1 > span").text());

}

$(document).ready(function(){

	/* 상품 리스트 이미지 위치 중앙으로 조정 */
	/*
	$(".Item").each(function(){
        var boxheight = $(this).find(".ImgBox").height();
		var imgheight = $(this).find(".Thumb").height();
		//alert("boxheight:"+boxheight+"  imgheight:"+imgheight);
		var boxheight2 = boxheight / 2;
		var imgheight2 = imgheight / 2;
		
		var imgposition = boxheight2 - imgheight2;

		//alert(imgposition);
		$(this).find("img.Thumb").css("top",imgposition);
    });
	*/
	
	/* 상품 상세 줌 이미지 보기 */
	var pqpq = $("#uifBigImg").click(function() {
		$("#uifBigImgZoom").attr("src",$(this).attr("src"));
	});

	/* 상단 메뉴 Fixed */
	$( window ).scroll(function() {
		var TopVal = $( window ).scrollTop();
		var TopFixed1 = 133;
		var TopFixed2 = 303;
		//$("#a18").text(topvalue);
		if( TopFixed1 <= TopVal){
			$("#UIF_HeaderWrap_Root").addClass("ScrollC_ON");
		}else{
			$("#UIF_HeaderWrap_Root").removeClass("ScrollC_ON");
		}
	});

	/* 우측 메뉴 */
	var openQuick = setTimeout( function() { classie.toggle ( UIF_RightArea , 'RightOpen' ); }, 3000 );
	var closeQuick = setTimeout( function() { classie.toggle ( UIF_RightArea , 'RightOpen' ); }, 5000 );
	$( ".RightAreaBtn" ).click( function(){ 
			clearTimeout(openQuick);
			clearTimeout(closeQuick);
			classie.toggle( UIF_RightArea , 'RightOpen' );
	});

	/* 탑 링크 */
    $('#UIF_TopLink').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
    });
	$( window ).scroll(function() {
		var TopLink = $( window ).scrollTop();
		if( TopLink < 100 ) {
			classie.remove( UIF_TopLink , 'LinkView' );
		}else{
			classie.add( UIF_TopLink , 'LinkView' );
		}
	});

});
